/*
Write a precursor to a menu-driven program.The program should display a menu
offering four choices, each labeled with a letter. If the user responds with a letter
other than one of the four valid choices, the program should prompt the user to
enter a valid response until the user complies.Then the program should use a
switch to select a simple action based on the user’s selection.A program run could
look something like this:
Please enter one of the following choices:
c) carnivore p) pianist
t) tree g) game
f
Please enter a c, p, t, or g: q
Please enter a c, p, t, or g: t
A maple is a tree.
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    char myChoice = '\n';

    do
    {
        cout << "Please enter one of the following choices: " << endl;
        cout << "c) carnivore   p) pianist" << endl;
        cout << "t) tree        g) game" << endl;
        cout << "q) quit" << endl;
        cout << "Selection: ";
        cin >> myChoice;
        
        switch(myChoice)
        {
            case 'c':
            case 'C':
                cout << "A lion is a carnivore" << endl;
                break;
            case 'p':
            case 'P':
                cout << "Mozart was a pianist" << endl;
                break;
            case 't':
            case 'T':
                cout << "A maple is a tree" << endl;
                break;
            case 'g':
            case 'G':
                cout << "Pokemon is a game" << endl;
                break;
            case 'q':
            case 'Q':
                cout << "Quitting..." << endl;
            default:
                cout << "Invalid input" << endl;
        };

    } while (myChoice != 'q' && myChoice != 'Q');
    
    return 0;
}