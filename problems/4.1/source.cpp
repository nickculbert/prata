/*
Write a C++ program that requests and displays information as shown in the following
example of output:
What is your first name? Betty Sue
What is your last name? Yewe
What letter grade do you deserve? B
What is your age? 22
Name: Yewe, Betty Sue
Grade: C
Age: 22
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

const int MAX_STR_LEN = 20;

int main()
{
    char firstName[MAX_STR_LEN] = {'\n'};
    char lastName[MAX_STR_LEN] = {'\n'};
    char grade = 'F';
    int age = 0;

    cout << "What is your first name: ";
    cin.getline(firstName, MAX_STR_LEN);
    cout << "What is your last name: ";
    cin.getline(lastName, MAX_STR_LEN);
    cout << "What is your age: ";
    cin >> age;
    cin.get();  //discard int new line
    cout << "What grade do you deserve: ";
    cin >> grade;

    cout << "Name: " << lastName << ", " << firstName << endl;
    cout << "Grade: " << char(grade + 1) << endl;
    cout << "Age: " << age << endl;

    return 0;
}