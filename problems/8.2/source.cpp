/*
The CandyBar structure contains three members.The first member holds the brand
name of a candy bar.The second member holds the weight (which may have a fractional
part) of the candy bar, and the third member holds the number of calories
(an integer value) in the candy bar.

Write a program that uses a function that takes as arguments a reference to CandyBar, 
a pointer-to-char, a double, and an int and uses the last three values to set the 
corresponding members of the structure.

The last three arguments should have default values of “Millennium Munch,” 2.85, and 350.

Also the program should use a function that takes a reference to a CandyBar as an
argument and displays the contents of the structure. Use const where appropriate.
*/

#include <iostream>
#include <string.h>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

struct candyBar
{
    string maker;
    double weight;
    int calories;
};

void setBar(candyBar &, string brand = "Millenium Munch", double w = 2.85, int c = 350);
void displayBar(const candyBar &);

int main()
{
    candyBar mine{"Default", 0,0};

    displayBar(mine);
    setBar(mine);
    displayBar(mine);




    return 0;
}

void setBar(candyBar & myBar, string brand, double w, int c)
{
    myBar.maker = brand;
    myBar.weight = w;
    myBar.calories = c;
}

void displayBar(const candyBar &myBar)
{
    cout << "Maker: " << myBar.maker << endl;
    cout << "Weight: " << myBar.weight << endl;
    cout << "Calories: " << myBar.calories << endl;
}