/*
Write a program that reads keyboard input to the @ symbol and that echoes the
input except for digits, converting each uppercase character to lowercase, and vice
versa. (Don’t forget the cctype family.)
*/

#include <iostream>
#include <cctype>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    char current = '\n';
    cout << "Enter a string of characters, @ acts as the terminator: ";
    cin >> current;
    while(current != '@')
    {
        if(isupper(current))
            cout << char(tolower(current));
        else if(islower(current))
            cout << char(toupper(current));
        else if(isspace(current))
            cout << " ";

        cin.get(current);
    }
    cout << endl;

    return 0;
}