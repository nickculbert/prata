/*
Write a program that asks the user to enter his or her first name and then last
name, and that then constructs, stores, and displays a third string, consisting of the
user’s last name followed by a comma, a space, and first name. Use char arrays and
functions from the cstring header file.A sample run could look like this:
Enter your first name: Flip
Enter your last name: Fleming
Here’s the information in a single string: Fleming, Flip
*/

#include <iostream>
#include <cstring>

using std::cin;
using std::cout;
using std::endl;

const int MAX_STR_LEN = 30;

int main()
{
    char firstname[MAX_STR_LEN] = {'\n'};
    char lastname[MAX_STR_LEN] = {'\n'};
    char fullname[MAX_STR_LEN] = {'\n'};

    cout << "Enter first name: ";
    cin.getline(firstname,MAX_STR_LEN);
    cout << "Enter last name: ";
    cin.getline(lastname, MAX_STR_LEN);
    strcpy(fullname, lastname);
    strcat(fullname, ", ");
    strcat(fullname, firstname);

    cout << "full name: " << fullname << endl;

    return 0;
}