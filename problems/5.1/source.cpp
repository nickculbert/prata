/*
Write a program that requests the user to enter two integers.The program should
then calculate and report the sum of all the integers between and including the two
integers. At this point, assume that the smaller integer is entered first. For example, if
the user enters 2 and 9, the program should report that the sum of all the integers
from 2 through 9 is 44.
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int num1 = 0;
    int num2 = 0;
    int sum = 0;

    cout << "This program sums the numbers between one int and a second int" << endl;
    cout << "Enter int 1: ";
    cin >> num1;
    cout << "Enter int 2: ";
    cin >> num2;

    for(int i = num1; i <= num2; i++)
    {
        sum = sum + i;
    }

    cout << "Sum: " << sum << endl;

    return 0;
}