/*
The CandyBar structure contains three members, as described in Programming
Exercise 5.Write a program that creates an array of three CandyBar structures, initializes
them to values of your choice, and then displays the contents of each structure.
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

const int MAX_STR_LEN = 30;

struct Candybar
{
    char brand[MAX_STR_LEN] = {"Default"};
    double weight = 0.;
    int calories = 0;
};

int main()
{
    Candybar mybars[3] = {"Mocha Munch", 2.3, 230,
                            "Kit Kat", 3.1, 410,
                            "Oh Henry", 4.9, 823};

    cout << "Candy bar data: " << endl;
    cout << "Bar 1:" << endl;
    cout << "Brand: " << mybars[0].brand << endl;
    cout << "Weight: " << mybars[0].weight << endl;
    cout << "Calories: " << mybars[0].calories << endl << endl;

    cout << "Bar 2:" << endl;
    cout << "Brand: " << mybars[1].brand << endl;
    cout << "Weight: " << mybars[1].weight << endl;
    cout << "Calories: " << mybars[1].calories << endl << endl;

    cout << "Bar 1:" << endl;
    cout << "Brand: " << mybars[2].brand << endl;
    cout << "Weight: " << mybars[2].weight << endl;
    cout << "Calories: " << mybars[2].calories << endl;

    return 0;
}