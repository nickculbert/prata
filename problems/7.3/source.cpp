/*
Here is a structure declaration:
struct box
{
char maker[40];
float height;
float width;
float length;
float volume;
};

a. Write a function that passes a box structure by value and that displays the
value of each member.

b. Write a function that passes the address of a box structure and that sets the
volume member to the product of the other three dimensions.

c. Write a simple program that uses these two functions.
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

struct box
{
char maker[40];
float height;
float width;
float length;
float volume;
};

void displayBox(const box);
void setBoxVol(box*);

int main()
{
    box myBox = {"Box CO", 2,3,4,0};

    displayBox(myBox);
    setBoxVol(&myBox);
    displayBox(myBox);

    return 0;
}

void displayBox(const box mybox)
{
    cout << "Box info: " << endl;
    cout << "Maker: " << mybox.maker << endl;
    cout << "Height: " << mybox.height << endl;
    cout << "Width: " << mybox.width << endl;
    cout << "Length: " << mybox.length << endl;
    cout << "Volume: " << mybox.volume << endl;
}

void setBoxVol(box *mybox)
{
    mybox->volume = mybox->height * mybox->length * mybox->width;
}
