/*
Write a C++ program that uses three user-defined functions (counting main() as
one) and produces the following output:
Three blind mice
Three blind mice
See how they run
See how they run
*/

#include <iostream>

void TBM();
void SHTR();

int main()
{
    TBM();
    TBM();
    SHTR();
    SHTR();

    return 0;
}

void TBM()
{
    std::cout << "Three blind mice" << std::endl;
}

void SHTR()
{
    std::cout << "See how they run" << std::endl;
}