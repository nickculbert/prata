/*
William Wingate runs a pizza-analysis service. For each pizza, he needs to record
the following information:
n The name of the pizza company, which can consist of more than one word
n The diameter of the pizza
n The weight of the pizza
Devise a structure that can hold this information and write a program that uses a
structure variable of that type.The program should ask the user to enter each of the
preceding items of information, and then the program should display that information.
Use cin (or its methods) and cout.
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

const int MAX_STR_LEN = 40;

struct Pizza
{
    char brand[MAX_STR_LEN] = "Default";
    int diameter = 0;
    double weight = 0.;
};

int main()
{
    Pizza myPie;

    cout << "Enter pizza brand: ";
    cin.getline(myPie.brand, MAX_STR_LEN);
    cout << "Enter pizza diameter: ";
    cin >> myPie.diameter;
    cout << "Enter pizza weight: ";
    cin >> myPie.weight;
    cin.get();  //discard newline

    cout << "Pizza information:" << endl;
    cout << "Brand: " << myPie.brand << endl;
    cout << "Diameter: " << myPie.diameter << endl;
    cout << "Weight: " << myPie.weight << endl;

    return 0;
}