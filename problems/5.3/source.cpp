/*
Write a program that asks the user to type in numbers.After each entry, the program
should report the cumulative sum of the entries to date.The program should
terminate when the user enters 0.
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    double myNum = 0.;
    double sum = 0.;

    do
    {
        cout << "Enter a number to add to sum: ";
        cin >> myNum;
        sum = sum + myNum;
        cout << "Running sum: " << sum << endl;
    } while (myNum != 0);
    

    return 0;
}