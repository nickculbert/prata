#include <iostream>
#include "sales.h"


// copies the lesser of 4 or n items from the array ar
// to the sales member of s and computes and stores the
// average, maximum, and minimum values of the entered items;
// remaining elements of sales, if any, set to 0
void SALES::setSales(Sales & s, const double ar[], int n)
{
    //double avg = 0.;
    s.max = ar[0];
    s.min = ar[0];
    s.average = 0.;

    for(int i = 0; i < n; i++)
    {
        s.sales[i] = ar[i];
        s.average = s.average + ar[i];
        if(s.max < ar[i])
            s.max = ar[i];
        if(s.min > ar[i])
            s.min = ar[i];
    }

    for(int i = n; i < 4; i++)
    {
        s.sales[i] = 0;
    }

    s.average = s.average / n;
    
}


// gathers sales for 4 quarters interactively, stores them
// in the sales member of s and computes and stores the
// average, maximum, and minimum values
void SALES::setSales(Sales & s)
{
    //double sales[4] = {0.};
    
    for (int i = 0; i < 4; i++)
    {
        std::cout << "Enter sales for month " << i+1 << ": ";
        std::cin >> s.sales[i];
    }

    s.max = s.sales[0];
    s.min = s.sales[0];
    s.average = 0.;

    for(int i = 0; i < 4; i++)
    {
        //s.sales[i] = ar[i];
        s.average = s.average + s.sales[i];
        if(s.max < s.sales[i])
            s.max = s.sales[i];
        if(s.min > s.sales[i])
            s.min = s.sales[i];
    }

    s.average = s.average / 4;
}

void SALES::showSales(const Sales & s)
{
    std::cout << "Sales info: " << std::endl;
    std::cout << "Sales: ";
    for(int i = 0; i < 3; i++)
    {
        std::cout << s.sales[i] << ", ";
    }
    std::cout << s.sales[3] << std::endl;
    std::cout << "Min: " << s.min << std::endl;
    std::cout << "Max: " << s.max << std::endl;
    std::cout << "Avg: " << s.average << std::endl;
}