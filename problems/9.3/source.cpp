#include <iostream>
#include "sales.h"

int main()
{
    SALES::Sales s1;
    SALES::Sales s2;
    double mysales[4] = {1,2,3,0};
    SALES::setSales(s1, mysales, 3);
    SALES::setSales(s2);
    SALES::showSales(s1);
    SALES::showSales(s2);
    return 0;
}