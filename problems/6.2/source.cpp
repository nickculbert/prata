/*
Write a program that reads up to 10 donation values into an array of double. (Or, if
you prefer, use an array template object.) The program should terminate input on
non-numeric input. It should report the average of the numbers and also report
how many numbers in the array are larger than the average.
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

const int MAX_ARR_SIZE = 10;

int main()
{
    double donations[MAX_ARR_SIZE];
    int validDonations = 0;

    cout << "Enter donations: " << endl;
    for(int i = 0; i < MAX_ARR_SIZE; i++)
    {
        cout << "Donation #" << i+1 << ": ";
        cin >> donations[i];
        if(cin.fail())
        {
            cout << "Invalid input detected..." << endl;
            break;
        }
        validDonations++;
    }
    cin.clear();

    //calculate average of donations
    double avg = 0.;
    for(int i = 0; i < validDonations; i++)
    {
        avg = avg + donations[i];
    }
    avg = avg / validDonations;
    cout << "Average donation: " << avg << endl;

    //determine number of donations larger than avg
    int largerThanAvg = 0;
    for(int i = 0; i < validDonations; i++)
    {
        if(donations[i] > avg)
            largerThanAvg++;
    }
    cout << "Number of donations larger than avg: " << largerThanAvg << endl;

    return 0;
}