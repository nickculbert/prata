/*
You sell the book C++ for Fools.Write a program that has you enter a year’s worth
of monthly sales (in terms of number of books, not of money).The program should
use a loop to prompt you by month, using an array of char * (or an array of
string objects, if you prefer) initialized to the month strings and storing the input
data in an array of int.Then, the program should find the sum of the array contents
and report the total sales for the year.
*/

#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

const string MONTHS[12] = {"January", "Febuary", "March", "April", "May", "June", "July",
                            "August", "September", "Octover", "November", "December"};

int main()
{
    int sales[12] = {0};
    int totalSales = 0;

    for(int i = 0; i < 12; i++)
    {
        cout << "Enter sales for the month of " << MONTHS[i] << ": ";
        cin >> sales[i];
        if(cin.fail())
        {
            cin.clear();
            break;
        }
    }

    for(int i = 0; i < 12; i++)
    {
        totalSales = totalSales + sales[i];
    }
    cout << "Total sales: " << totalSales << endl;

    return 0;
}