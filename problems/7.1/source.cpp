/*
Write a program that repeatedly asks the user to enter pairs of numbers until at
least one of the pair is 0. For each pair, the program should use a function to calculate
the harmonic mean of the numbers.The function should return the answer to
main(), which should report the result.The harmonic mean of the numbers is the
inverse of the average of the inverses and can be calculated as follows:

harmonic mean = 2.0 × x × y / (x + y)
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

double hm(double, double);

int main()
{
    double a = 0.;
    double b = 0.;

    cout << "Enter 2 numbers to calculate the harmonic mean: " << endl;
    cout << "Num1: ";
    cin >> a;
    cout << "Num2: ";
    cin >> b;

    while(a != 0 && b != 0)
    {
        cout << "The harmonic mean of " << a << " and " << b << " is: " << hm(a,b) << endl;
        cout << "Enter 2 numbers to calculate the harmonic mean: " << endl;
        cout << "Num1: ";
        cin >> a;
        cout << "Num2: ";
        cin >> b;
    }
    cout << "0 input detected, terminating..." << endl;

    return 0;
}

double hm(double x, double y)
{
    return (2.0 * x * y)/(x + y);
}