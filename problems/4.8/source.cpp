/*
Do Programming Exercise 7 but use new to allocate a structure instead of declaring
a structure variable.Also have the program request the pizza diameter before it
requests the pizza company name.
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

const int MAX_STR_LEN = 40;

struct Pizza
{
    char brand[MAX_STR_LEN] = "Default";
    int diameter = 0;
    double weight = 0.;
};

int main()
{
    Pizza * myPie = new Pizza;

    cout << "Enter pizza brand: ";
    cin.getline(myPie->brand, MAX_STR_LEN);
    cout << "Enter pizza diameter: ";
    cin >> myPie->diameter;
    cout << "Enter pizza weight: ";
    cin >> myPie->weight;
    cin.get();  //discard newline

    cout << "Pizza information:" << endl;
    cout << "Brand: " << myPie->brand << endl;
    cout << "Diameter: " << myPie->diameter << endl;
    cout << "Weight: " << myPie->weight << endl;

    delete myPie;

    return 0;
}