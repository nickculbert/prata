/*
Write a C++ program that displays your name and address (or if you value your
privacy, a fictitious name and address).
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    cout << "Name: John Doe" << endl;
    cout << "Address: 123 fake st" << endl;

    return 0;
}