#include <iostream>
#include "golf.h"

using std::cin;
using std::cout;
using std::endl;

int main()
{
    golf ann;
    setgolf(ann, "Ann Birdfree", 24);

    golf andy;
    setgolf(andy);

    showgolf(ann);
    handicap(ann,15);
    showgolf(ann);

    showgolf(andy);

    return 0;
}