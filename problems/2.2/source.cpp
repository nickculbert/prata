/*
Write a C++ program that asks for a distance in furlongs and converts it to yards.
(One furlong is 220 yards.)
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

const int YARDS_IN_FURLONG = 220;

int main()
{
    int furlongs = 0;
    cout << "Enter a distance in furlongs: ";
    cin >> furlongs;
    cout << furlongs << " furlongs = " << furlongs * YARDS_IN_FURLONG << " yards" << endl;

    return 0;
}