/*
The CandyBar structure contains three members.The first member holds the brand
name of a candy bar.The second member holds the weight (which may have a fractional
part) of the candy bar, and the third member holds the number of calories
(an integer value) in the candy bar.Write a program that declares such a structure
and creates a CandyBar variable called snack, initializing its members to "Mocha
Munch", 2.3, and 350, respectively.The initialization should be part of the declaration
for snack. Finally, the program should display the contents of the snack variable.
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

const int MAX_STR_LEN = 30;

struct Candybar
{
    char brand[MAX_STR_LEN] = {"Default"};
    double weight = 0.;
    int calories = 0;
};

int main()
{
    Candybar mybar = {"Mocha Munch", 2.3, 350};
    cout << "Candybar info:" << endl;
    cout << "Brand: " << mybar.brand << endl;
    cout << "Weight: " << mybar.weight << endl;
    cout << "Calories: " << mybar.calories << endl;

    return 0;
}