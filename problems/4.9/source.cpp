/*
Do Programming Exercise 6, but instead of declaring an array of three CandyBar
structures, use new to allocate the array dynamically.
*/

#include <iostream>
#include <cstring>

using std::cin;
using std::cout;
using std::endl;

const int MAX_STR_LEN = 30;

struct Candybar
{
    char brand[MAX_STR_LEN] = {"Default"};
    double weight = 0.;
    int calories = 0;
};

int main()
{
    Candybar * mybars = new Candybar[3];
    
    strcpy(mybars[0].brand, "Mocha Munch");
    mybars[0].calories = 310;
    mybars[0].weight = 3.6;

    strcpy(mybars[1].brand, "Kit Kat");
    mybars[1].calories = 230;
    mybars[1].weight = 2.7;

    strcpy(mybars[2].brand, "Oh Henry");
    mybars[2].calories = 561;
    mybars[2].weight = 4.9;

    cout << "Candy bar data: " << endl;
    cout << "Bar 1:" << endl;
    cout << "Brand: " << mybars[0].brand << endl;
    cout << "Weight: " << mybars[0].weight << endl;
    cout << "Calories: " << mybars[0].calories << endl << endl;

    cout << "Bar 2:" << endl;
    cout << "Brand: " << mybars[1].brand << endl;
    cout << "Weight: " << mybars[1].weight << endl;
    cout << "Calories: " << mybars[1].calories << endl << endl;

    cout << "Bar 1:" << endl;
    cout << "Brand: " << mybars[2].brand << endl;
    cout << "Weight: " << mybars[2].weight << endl;
    cout << "Calories: " << mybars[2].calories << endl;

    delete [] mybars;

    return 0;
}