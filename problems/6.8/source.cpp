/*
Write a program that opens a text file, reads it character-by-character to the end of
the file, and reports the number of characters in the file.
*/

#include <iostream>
#include <fstream>
#include <cstdlib> // support for exit()

using std::cout;
using std::endl;
using std::cin;

const int SIZE = 60;

int main()
{
    char filename[SIZE];
    std::ifstream inFile; // object for handling file input
    cout << "Enter name of data file: ";
    cin.getline(filename, SIZE);
    inFile.open(filename); // associate inFile with a file
    
    if (!inFile.is_open()) // failed to open file
    {
        cout << "Could not open the file " << filename << endl;
        cout << "Program terminating.\n";
        exit(EXIT_FAILURE);
    }

    char current = '\n';

    int count = 0;
    inFile.get(current);
    while (inFile.good()) // while input good and not at EOF
    {
        cout << current;
        ++count; // one more item read
        inFile.get(current); // get next value
    }
    cout << endl;

    if (inFile.eof())
        cout << "End of file reached.\n";
    else if (inFile.fail())
        cout << "Input terminated by data mismatch.\n";
    else
        cout << "Input terminated for unknown reason.\n";

    cout << "Total Characters read: " << count;

    inFile.close();

    return 0;
}