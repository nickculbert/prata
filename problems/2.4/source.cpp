/*
Write a program that asks the user to enter his or her age.The program then should
display the age in months:
Enter your age: 29
Your age in months is 384.
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int age = 0;
    cout << "Enter age in years: ";
    cin >> age;
    cout << "Your age in months: " << age * 12 << endl;


    return 0;
}