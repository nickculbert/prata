/*
Write a program that asks the user to enter up to 10 golf scores, which are to be
stored in an array.You should provide a means for the user to terminate input prior
to entering 10 scores.The program should display all the scores on one line and
report the average score. Handle input, display, and the average calculation with
three separate array-processing functions.
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int inputScore(int scores[]);
double avgScores(const int scores[], int n);
void displayScores(const int scores[], int n);

///////////////////////////////////////////////////////////////////////////
int main()
{
    int scores[10] = {0};
    int validScores = inputScore(scores);

    displayScores(scores, validScores);

    cout << "Avg score: " << avgScores(scores, validScores);

    return 0;
}


//////////////////////////////////////////////////////////////////////////
int inputScore(int scores[])
{
    //int s[10] = {0};
    int valid = 0;
    int placeholder = 0;
    cout << "Enter up to 10 scores, enter a 0 to quit" << endl;
    for(int i = 0; i < 10; i++)
    {
        cout << "Enter score #" << i+1 << ": ";
        cin >> placeholder;
        if(placeholder == 0 || cin.fail())
        {
            cin.clear();
            break;
        }
        else
        {
            scores[i] = placeholder;
            valid++;
        }
    }

    return valid;
}

double avgScores(const int scores[], int n)
{
    double avg = 0.;
    for(int i = 0; i < n; i++)
    {
        avg = avg + scores[i];
    }
    return avg / n;
}

void displayScores(const int scores[], int n)
{
    cout << "Scores: ";
    for(int i = 0; i < n; i++)
    {
        cout << scores[i];
        if(i < n-1)
        {
            cout << ", ";
        }
    }
    cout << endl;
}